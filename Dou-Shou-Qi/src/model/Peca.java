/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author douglas
 */
public abstract class Peca {

    private int forca;
    private String srcImg;
    protected String nome;
    public Tabuleiro tabuleiro;
    protected int posX;
    protected int posY;
    private Jogador jogador;
    private boolean podeMovimentar; //atributo necessário para verificar se a peça pode ser movimentada
    private boolean estaViva;

    public Peca(String nome, int forca, String srcImg, Tabuleiro tabuleiro, int posX, int posY, Jogador jogador) {
        this.nome = nome;
        this.forca = forca;
        this.srcImg = srcImg;
        this.tabuleiro = tabuleiro;
        this.posX = posX;
        this.posY = posY;
        this.jogador = jogador;
        this.podeMovimentar = true;
        this.estaViva = true;
    }

    public void movimentar(int x, int y) {
        this.tabuleiro.getTabuleiroEspaco()[this.getPosX()][this.getPosY()].setPeca(null);
        this.setPosX(x);
        this.setPosY(y);
        this.tabuleiro.getTabuleiroEspaco()[x][y].setPeca(this);
    }

//    public boolean podeMovimentar(int x, int y) {
//
//        return (((((x == posX + 1) && (y == posY)) || ((x == posX) && (y == posY + 1)))
//                || (((x == posX - 1) && (y == posY)) || ((x == posX) && (y == posY - 1))))
//                && ((!this.nome.equalsIgnoreCase("Rato")) && (!tabuleiro.getTabuleiroEspaco()[x][y].getNome().equalsIgnoreCase("lago")))
//                ) && (!tabuleiro.getTabuleiroEspaco()[x][y].getNome().equalsIgnoreCase("toca"));
//
//    }
    public int getForca() {
        return forca;
    }

    public String getSrcImg() {
        return srcImg;
    }

    public String getNome() {
        return nome;
    }

    public Tabuleiro getTabuleiro() {
        return tabuleiro;
    }

    public int getPosX() {
        return posX;
    }

    public void setPosX(int posX) {
        this.posX = posX;
    }

    public int getPosY() {
        return posY;
    }

    public void setPosY(int posY) {
        this.posY = posY;
    }

    public void setForca(int forca) {
        this.forca = forca;
    }

    public boolean getPodeMovimentar() {
        return podeMovimentar;
    }

    public void setPodeMovimentar(boolean podeMovimentar) {
        this.podeMovimentar = podeMovimentar;
    }

    public boolean isEstaViva() {
        return estaViva;
    }

    public void setEstaViva(boolean estaViva) {
        this.estaViva = estaViva;
    }

}
