/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author douglas
 */
public class ConcretJogador1 extends BuilderJogador {

    public ConcretJogador1(FabricaDePeca fabrica) {
        super(fabrica);
    }

    //Adicionando as peças do jogador1 no jogador1
    @Override
    public void constroiElefante(Tabuleiro tabuleiro) {
        jogador.addPeca(fabricaDePeca.criarElefante("Elefante", 8, "/view/imagens/elefante1.png", tabuleiro, 2, 0, jogador));
    }

    @Override
    public void constroiTigre(Tabuleiro tabuleiro) {
        jogador.addPeca(fabricaDePeca.criarTigre("Tigre", 6, "/view/imagens/tigre1.png", tabuleiro, 0, 0, jogador));
    }

    @Override
    public void constroiLeao(Tabuleiro tabuleiro) {
        jogador.addPeca(fabricaDePeca.criarLeao("Leao", 7, "/view/imagens/leao1.png", tabuleiro, 0, 6, jogador));
    }

    @Override
    public void constroiLobo(Tabuleiro tabuleiro) {
        jogador.addPeca(fabricaDePeca.criarLobo("Lobo", 4, "/view/imagens/lobo1.png", tabuleiro, 2, 2, jogador));
    }

    @Override
    public void constroiLeopardo(Tabuleiro tabuleiro) {
        jogador.addPeca(fabricaDePeca.criarLeopardo("Leopardo", 5, "/view/imagens/leopardo1.png", tabuleiro, 2, 4, jogador));
    }

    @Override
    public void constroiGato(Tabuleiro tabuleiro) {
        jogador.addPeca(fabricaDePeca.criarGato("Gato", 2, "/view/imagens/gato1.png", tabuleiro, 1, 1, jogador));
    }

    @Override
    public void constroiCachorro(Tabuleiro tabuleiro) {
        jogador.addPeca(fabricaDePeca.criarCachorro("Cachorro", 3, "/view/imagens/cao1.png", tabuleiro, 1, 5, jogador));
    }

    @Override
    public void constroiRato(Tabuleiro tabuleiro) {
        jogador.addPeca(fabricaDePeca.criarRato("Rato", 1, "/view/imagens/rato1.png", tabuleiro, 2, 6, jogador));
    }

    @Override
    public void construirArmadilhas(Tabuleiro tabuleiro) {
        jogador.addArmadilhas(tabuleiro.getTabuleiroEspaco()[0][2] = new Armadilha("armadilha", 0, 2, "/view/imagens/armadilha.PNG", null));
        jogador.addArmadilhas(tabuleiro.getTabuleiroEspaco()[0][4] = new Armadilha("armadilha", 0, 4, "/view/imagens/armadilha.PNG", null));
        jogador.addArmadilhas(tabuleiro.getTabuleiroEspaco()[1][3] = new Armadilha("armadilha", 1, 3, "/view/imagens/armadilha.PNG", null));
    }

    @Override
    public void construirToca(Tabuleiro tabuleiro) {
        jogador.setToca(tabuleiro.getTabuleiroEspaco()[0][3] = new Toca("toca", 0, 3, "/view/imagens/toca.png", null));
    }

}
