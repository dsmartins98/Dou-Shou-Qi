/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.decorator;

import model.Peca;

/**
 *
 * @author dougl
 */
public interface PermissaoMovimentacao {
 
    boolean permitir(Peca peca, int x, int y);
    
}
