/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author douglas
 */
public abstract class FabricaDePeca {

    public abstract Rato criarRato(String nome, int forca, String srcImg, Tabuleiro tabuleiro, int posX, int posY, Jogador jogador);

    public abstract Gato criarGato(String nome, int forca, String srcImg, Tabuleiro tabuleiro, int posX, int posY, Jogador jogador);

    public abstract Cachorro criarCachorro(String nome, int forca, String srcImg, Tabuleiro tabuleiro, int posX, int posY, Jogador jogador);

    public abstract Lobo criarLobo(String nome, int forca, String srcImg, Tabuleiro tabuleiro, int posX, int posY, Jogador jogador);

    public abstract Leopardo criarLeopardo(String nome, int forca, String srcImg, Tabuleiro tabuleiro, int posX, int posY, Jogador jogador);

    public abstract Tigre criarTigre(String nome, int forca, String srcImg, Tabuleiro tabuleiro, int posX, int posY, Jogador jogador);

    public abstract Leao criarLeao(String nome, int forca, String srcImg, Tabuleiro tabuleiro, int posX, int posY, Jogador jogador);

    public abstract Elefante criarElefante(String nome, int forca, String srcImg, Tabuleiro tabuleiro, int posX, int posY, Jogador jogador);

}
