/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.strategy;

import model.Espaco;
import model.Jogador;

/**
 *
 * @author dougl
 */
public class VerificaToca implements VerificarEspaco {

    @Override
    public boolean verificar(Jogador jogador, Espaco espaco) {
        try {
            if (jogador.getToca().getX() == espaco.getX() && jogador.getToca().getY() == espaco.getY()) {
                return false;
            }
        } catch (NullPointerException e) {
            return false;
        }

        return true;
    }

}
