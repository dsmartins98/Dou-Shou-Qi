/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.state;

import model.Jogador;
import model.Tabuleiro;

/**
 *
 * @author Douglas
 */
public class TabuleiroDesativado extends TabuleiroEstado {

    public TabuleiroDesativado(Tabuleiro aThis) {
        super(aThis);
    }

    @Override
    public void proxEstado() throws Exception {
        tabuleiro.setEstado(new TabuleiroAtivado(tabuleiro));
        tabuleiro.liberarPecas(true);
    }

    @Override
    public void liberarPecas(boolean liberar) throws Exception {
        throw new Exception("Peças desabilitadas");
    }

}
