/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.visitor;

import model.Jogador;
import model.Peca;

/**
 *
 * @author 10354209965
 */
public class CalcPecas implements Visitor {

    private int numeroPecas = 8;

    @Override
    public void visit(Peca peca) {
        if (!peca.isEstaViva()) {
            numeroPecas--;
        }
    }

    @Override
    public int getNumeroPecas() {
        return this.numeroPecas;
    }

}
