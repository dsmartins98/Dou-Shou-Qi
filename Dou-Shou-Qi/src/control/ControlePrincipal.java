/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Armadilha;
import model.BuilderJogador;
import model.CommandInvoker;
import model.ConcretJogador1;
import model.ConcretJogador2;
import model.Diretor;
import model.FabricaConcretaDePeca;
import model.Grama;
import model.Jogador;
import model.Lago;
import model.Peca;
import model.Tabuleiro;
import model.Toca;
import model.decorator.PermissaoEntraRio;
import model.decorator.PermissaoMovimentacao;
import model.decorator.PermissaoMovimentacaoPadrao;
import model.decorator.PermissaoPulaRio;
import model.strategy.VerificaArmadilha;
import model.strategy.VerificaPeca;
import model.strategy.VerificaToca;
import model.visitor.CalcPecas;
import model.visitor.Visitor;

/**
 *
 * @author douglas
 */
public class ControlePrincipal implements ControleInterface {

    private final List<Observador> observadores = new ArrayList<>();
    private Tabuleiro tabuleiro = Tabuleiro.getInstance();
    private Jogador jogador1;
    private Jogador jogador2;
    private BuilderJogador builder;
    private FabricaConcretaDePeca fabrica;
    private Diretor diretor;
    private Peca pecaCarregada;
    private int rodada;
    private CommandInvoker invoker;
    private String srcAntigo = "";
    private String srcNovo = "";
    private int xNovo = 0;
    private int yNovo = 0;
    private int xAntigo = 0;
    private int yAntigo = 0;
    private List<Peca> pecasNaArmadilhaJogador1 = new ArrayList<>();
    private List<Peca> pecasNaArmadilhaJogador2 = new ArrayList<>();

    public ControlePrincipal() {
        this.rodada = 0;
        fabrica = new FabricaConcretaDePeca();
        tabuleiro.start();
    }

    @Override
    public void addObservador(Observador obs) {
        observadores.add(obs);
    }

    @Override
    public void removeObservador(Observador obs) {
        observadores.add(obs);
    }

    // É neste método que se inicia a construção do tabuleiro
    @Override
    public void iniciarTabuleiro() {

        jogador1 = criarJogador1();
        jogador2 = criarJogador2();
        preparaTabuleiroInicial();
        try {
            tabuleiro.getEstado().proxEstado();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        observadores.get(0).alterarStatus("Status: Jogo Iniciado!");
        observadores.get(0).desabilitarBotaoStart();

    }

    private void preparaTabuleiroInicial() {

        //Coluna 1 - Começa construção inicial das peças no tabuleiro do Jogador1
        tabuleiro.getTabuleiroEspaco()[0][0] = new Grama("grama", 0, 0, "/view/imagens/grama.PNG", jogador1.getPeca("Tigre"));
        tabuleiro.getTabuleiroEspaco()[0][1] = new Grama("grama", 0, 1, "/view/imagens/grama.PNG", null);
        tabuleiro.getTabuleiroEspaco()[0][2] = new Armadilha("armadilha", 0, 2, "/view/imagens/armadilha.PNG", null);
        tabuleiro.getTabuleiroEspaco()[0][3] = new Toca("toca", 0, 3, "/view/imagens/toca.png", null);
        tabuleiro.getTabuleiroEspaco()[0][4] = new Armadilha("armadilha", 0, 4, "/view/imagens/armadilha.PNG", null);
        tabuleiro.getTabuleiroEspaco()[0][5] = new Grama("grama", 0, 5, "/view/imagens/grama.PNG", null);
        tabuleiro.getTabuleiroEspaco()[0][6] = new Grama("grama", 0, 6, "/view/imagens/grama.PNG", jogador1.getPeca("Leao"));

        //Coluna 2
        tabuleiro.getTabuleiroEspaco()[1][0] = new Grama("grama", 1, 0, "/view/imagens/grama.PNG", null);
        tabuleiro.getTabuleiroEspaco()[1][1] = new Grama("grama", 1, 1, "/view/imagens/grama.PNG", jogador1.getPeca("Gato"));
        tabuleiro.getTabuleiroEspaco()[1][2] = new Grama("grama", 1, 2, "/view/imagens/grama.PNG", null);
        tabuleiro.getTabuleiroEspaco()[1][3] = new Armadilha("armadilha", 1, 3, "/view/imagens/armadilha.PNG", null);
        tabuleiro.getTabuleiroEspaco()[1][4] = new Grama("grama", 1, 4, "/view/imagens/grama.PNG", null);
        tabuleiro.getTabuleiroEspaco()[1][5] = new Grama("grama", 1, 5, "/view/imagens/grama.PNG", jogador1.getPeca("Cachorro"));
        tabuleiro.getTabuleiroEspaco()[1][6] = new Grama("grama", 1, 6, "/view/imagens/grama.PNG", null);

        //Coluna 3 - Termina a construção inicial das peças no tabuleiro do Jogador1
        tabuleiro.getTabuleiroEspaco()[2][0] = new Grama("grama", 2, 0, "/view/imagens/grama.PNG", jogador1.getPeca("Elefante"));
        tabuleiro.getTabuleiroEspaco()[2][1] = new Grama("grama", 2, 1, "/view/imagens/grama.PNG", null);
        tabuleiro.getTabuleiroEspaco()[2][2] = new Grama("grama", 2, 2, "/view/imagens/grama.PNG", jogador1.getPeca("Lobo"));
        tabuleiro.getTabuleiroEspaco()[2][3] = new Grama("grama", 2, 3, "/view/imagens/grama.PNG", null);
        tabuleiro.getTabuleiroEspaco()[2][4] = new Grama("grama", 2, 4, "/view/imagens/grama.PNG", jogador1.getPeca("Leopardo"));
        tabuleiro.getTabuleiroEspaco()[2][5] = new Grama("grama", 2, 5, "/view/imagens/grama.PNG", null);
        tabuleiro.getTabuleiroEspaco()[2][6] = new Grama("grama", 2, 6, "/view/imagens/grama.PNG", jogador1.getPeca("Rato"));

        //Coluna 4
        tabuleiro.getTabuleiroEspaco()[3][0] = new Grama("grama", 3, 0, "/view/imagens/grama.PNG", null);
        tabuleiro.getTabuleiroEspaco()[3][1] = new Lago("lago", 3, 1, "/view/imagens/lago.png", null);
        tabuleiro.getTabuleiroEspaco()[3][2] = new Lago("lago", 3, 2, "/view/imagens/lago.png", null);
        tabuleiro.getTabuleiroEspaco()[3][3] = new Grama("grama", 3, 3, "/view/imagens/grama.PNG", null);
        tabuleiro.getTabuleiroEspaco()[3][4] = new Lago("lago", 3, 4, "/view/imagens/lago.png", null);
        tabuleiro.getTabuleiroEspaco()[3][5] = new Lago("lago", 3, 5, "/view/imagens/lago.png", null);
        tabuleiro.getTabuleiroEspaco()[3][6] = new Grama("grama", 3, 6, "/view/imagens/grama.PNG", null);

        //Coluna 5
        tabuleiro.getTabuleiroEspaco()[4][0] = new Grama("grama", 4, 0, "/view/imagens/grama.PNG", null);
        tabuleiro.getTabuleiroEspaco()[4][1] = new Lago("lago", 4, 1, "/view/imagens/lago.png", null);
        tabuleiro.getTabuleiroEspaco()[4][2] = new Lago("lago", 4, 2, "/view/imagens/lago.png", null);
        tabuleiro.getTabuleiroEspaco()[4][3] = new Grama("grama", 4, 3, "/view/imagens/grama.PNG", null);
        tabuleiro.getTabuleiroEspaco()[4][4] = new Lago("lago", 4, 4, "/view/imagens/lago.png", null);
        tabuleiro.getTabuleiroEspaco()[4][5] = new Lago("lago", 4, 5, "/view/imagens/lago.png", null);
        tabuleiro.getTabuleiroEspaco()[4][6] = new Grama("grama", 4, 6, "/view/imagens/grama.PNG", null);

        //Coluna 6 
        tabuleiro.getTabuleiroEspaco()[5][0] = new Grama("grama", 5, 0, "/view/imagens/grama.PNG", null);
        tabuleiro.getTabuleiroEspaco()[5][1] = new Lago("lago", 5, 1, "/view/imagens/lago.png", null);
        tabuleiro.getTabuleiroEspaco()[5][2] = new Lago("lago", 5, 2, "/view/imagens/lago.png", null);
        tabuleiro.getTabuleiroEspaco()[5][3] = new Grama("grama", 5, 3, "/view/imagens/grama.PNG", null);
        tabuleiro.getTabuleiroEspaco()[5][4] = new Lago("lago", 5, 4, "/view/imagens/lago.png", null);
        tabuleiro.getTabuleiroEspaco()[5][5] = new Lago("lago", 5, 5, "/view/imagens/lago.png", null);
        tabuleiro.getTabuleiroEspaco()[5][6] = new Grama("grama", 5, 6, "/view/imagens/grama.PNG", null);

        //Coluna 7 - Começa construção inicial das peças no tabuleiro do Jogador2
        tabuleiro.getTabuleiroEspaco()[6][0] = new Grama("grama", 6, 0, "/view/imagens/grama.PNG", jogador2.getPeca("Rato"));
        tabuleiro.getTabuleiroEspaco()[6][1] = new Grama("grama", 6, 1, "/view/imagens/grama.PNG", null);
        tabuleiro.getTabuleiroEspaco()[6][2] = new Grama("grama", 6, 2, "/view/imagens/grama.PNG", jogador2.getPeca("Leopardo"));
        tabuleiro.getTabuleiroEspaco()[6][3] = new Grama("grama", 6, 3, "/view/imagens/grama.PNG", null);
        tabuleiro.getTabuleiroEspaco()[6][4] = new Grama("grama", 6, 4, "/view/imagens/grama.PNG", jogador2.getPeca("Lobo"));
        tabuleiro.getTabuleiroEspaco()[6][5] = new Grama("grama", 6, 5, "/view/imagens/grama.PNG", null);
        tabuleiro.getTabuleiroEspaco()[6][6] = new Grama("grama", 6, 6, "/view/imagens/grama.PNG", jogador2.getPeca("Elefante"));

        //Coluna 8
        tabuleiro.getTabuleiroEspaco()[7][0] = new Grama("grama", 7, 0, "/view/imagens/grama.PNG", null);
        tabuleiro.getTabuleiroEspaco()[7][1] = new Grama("grama", 7, 1, "/view/imagens/grama.PNG", jogador2.getPeca("Cachorro"));
        tabuleiro.getTabuleiroEspaco()[7][2] = new Grama("grama", 7, 2, "/view/imagens/grama.PNG", null);
        tabuleiro.getTabuleiroEspaco()[7][3] = new Armadilha("armadilha", 7, 3, "/view/imagens/armadilha.PNG", null);
        tabuleiro.getTabuleiroEspaco()[7][4] = new Grama("grama", 7, 4, "/view/imagens/grama.PNG", null);
        tabuleiro.getTabuleiroEspaco()[7][5] = new Grama("grama", 7, 5, "/view/imagens/grama.PNG", jogador2.getPeca("Gato"));
        tabuleiro.getTabuleiroEspaco()[7][6] = new Grama("grama", 7, 6, "/view/imagens/grama.PNG", null);

        //Coluna 9 - Termina a construção inicial das peças no tabuleiro do Jogador1
        tabuleiro.getTabuleiroEspaco()[8][0] = new Grama("grama", 8, 0, "/view/imagens/grama.PNG", jogador2.getPeca("Leao"));
        tabuleiro.getTabuleiroEspaco()[8][1] = new Grama("grama", 8, 1, "/view/imagens/grama.PNG", null);
        tabuleiro.getTabuleiroEspaco()[8][2] = new Armadilha("armadilha", 8, 2, "/view/imagens/armadilha.PNG", null);
        tabuleiro.getTabuleiroEspaco()[8][3] = new Toca("toca", 8, 3, "/view/imagens/toca.png", null);
        tabuleiro.getTabuleiroEspaco()[8][4] = new Armadilha("armadilha", 8, 4, "/view/imagens/armadilha.PNG", null);
        tabuleiro.getTabuleiroEspaco()[8][5] = new Grama("grama", 8, 5, "/view/imagens/grama.PNG", null);
        tabuleiro.getTabuleiroEspaco()[8][6] = new Grama("grama", 8, 6, "/view/imagens/grama.PNG", jogador2.getPeca("Tigre"));

    }

    private Jogador criarJogador1() {
        builder = new ConcretJogador1(fabrica);
        diretor = new Diretor(builder);
        diretor.construir(tabuleiro);
        return builder.getJogador();
    }

    private Jogador criarJogador2() {
        builder = new ConcretJogador2(fabrica);
        diretor = new Diretor(builder);
        diretor.construir(tabuleiro);
        return builder.getJogador();
    }

    @Override
    public void carregarEspaco(int valorPosicao) {

//        System.out.println("Peças Jogador 1:" + jogador1.getPecas().size());
//        System.out.println("Peças Jogador 2:" + jogador2.getPecas().size());
        if (tabuleiro.isHabilitado()) {

            String[] valoresSeparados = retornaValorInteiro(valorPosicao);

            //valor I (varia de 0 a 6)
            int i = Integer.parseInt(valoresSeparados[1]);

            //valor k (varia de 0 a 8)
            int k = Integer.parseInt(valoresSeparados[0]);

            Peca peca = null;

            //Verifica primeiro se no espaco escolhido há uma peça
            if (tabuleiro.getTabuleiroEspaco()[k][i].getPeca() != null) {
                if (tabuleiro.getTabuleiroEspaco()[k][i].getPeca().getPodeMovimentar()) {
                    peca = tabuleiro.getTabuleiroEspaco()[k][i].getPeca();

                    //verifica se é a rodada do JOGADOR1
                    if (this.rodada == 0) {
                        //verifica se jogador1 clicou em uma peca dele mesmo

                        //LOGO ABAIXO VAI O STRATEGY
                        boolean verificarPeca = jogador1.verificarEspacoEscolhido(new VerificaPeca(), tabuleiro.getTabuleiroEspaco()[k][i]);
                        //---- FIM STRATEGY ---

                        if (verificarPeca) {
                            //verifica se peça dele pode ser movimentada
                            this.pecaCarregada = peca;
                            this.srcAntigo = tabuleiro.getTabuleiroEspaco()[k][i].getSrcImg();
                            this.xAntigo = k;
                            this.yAntigo = i;
                            return;
                        } else {

                            if (pecaCarregada != null) {
                                PermissaoMovimentacao permissao = retornaPermissao(pecaCarregada);

                                if (!(pecaCarregada.getNome().equalsIgnoreCase("Elefante") && peca.getNome().equalsIgnoreCase("Rato"))) {
                                    if (permissao.permitir(pecaCarregada, k, i) && (pecaCarregada.getForca() >= peca.getForca() || (pecaCarregada.getNome().equalsIgnoreCase("Rato") && peca.getNome().equalsIgnoreCase("Elefante")))) {

                                        ordemMovimentar(pecaCarregada, k, i, 1, pecaCarregada.getForca());
                                        liberarPecas(pecasNaArmadilhaJogador1, pecaCarregada);
                                        peca.setEstaViva(false);

                                        //LOGO ABAIXO VAI O STRATEGY
                                        boolean verificarArmadilhaStrategy = jogador1.verificarEspacoEscolhido(new VerificaArmadilha(), tabuleiro.getTabuleiroEspaco()[k][i]);
                                        // ----- FIM DO STRATEGY -------
                                        if (verificarArmadilhaStrategy && tabuleiro.getTabuleiroEspaco()[k][i].getNome().equalsIgnoreCase("armadilha")) {
                                            pecasNaArmadilhaJogador1.add(pecaCarregada);
                                            System.out.println("Entrou na lista");
                                        }

                                        //removendo a peça capturada
                                        //  jogador2.removePeca(peca);
                                        // VISITOR
                                        Visitor calcPecas = new CalcPecas();
                                        jogador2.accept(calcPecas);
//                                        System.out.println("Jogador 2 - Peças: " + jogador2.getPecas().size());
//                                        System.out.println("JOGADOR 2 - NUMERO DE PEÇAS: " + calcPecas.getNumeroPecas());
                                        //jogador2.getPecas().isEmpty()
                                        if (calcPecas.getNumeroPecas() == 0) {
                                            try {
                                                tabuleiro.getEstado().proxEstado();
                                                observadores.get(0).alterarStatus("Jogo Finalizado! Você ganhou!");
                                            } catch (Exception ex) {
//                                                    Logger.getLogger(ControlePrincipal.class.getName()).log(Level.SEVERE, null, ex);
                                            }
                                            alertaJogoFinalizado("Jogo Finalizado! Você ganhou!");
                                        }
                                        return;
                                    }
                                }

                                pecaCarregada = null;
                            }

                        }
                    } else {
                        //verifica se jogador2 clicou em uma peca dele mesmo
                        //LOGO ABAIXO VAI O STRATEGY
                        boolean verificarPeca = (boolean) jogador2.verificarEspacoEscolhido(new VerificaPeca(), tabuleiro.getTabuleiroEspaco()[k][i]);
                        //---- FIM STRATEGY ---

                        if (verificarPeca) {
                            //verifica se peça dele pode ser movimentada
//                        if (pecaLista(pecasNaArmadilhaJogador2, peca)) {
                            this.pecaCarregada = peca;
                            this.srcAntigo = tabuleiro.getTabuleiroEspaco()[k][i].getSrcImg();
                            this.xAntigo = k;
                            this.yAntigo = i;
                            return;
//                        }
                        } else {

                            if (pecaCarregada != null) {
                                PermissaoMovimentacao permissao = retornaPermissao(pecaCarregada);
                                if (!(pecaCarregada.getNome().equalsIgnoreCase("Elefante") && peca.getNome().equalsIgnoreCase("Rato"))) {
                                    if (permissao.permitir(pecaCarregada, k, i) && (pecaCarregada.getForca() >= peca.getForca() || (pecaCarregada.getNome().equalsIgnoreCase("Rato") && peca.getNome().equalsIgnoreCase("Elefante")))) {

                                        ordemMovimentar(pecaCarregada, k, i, 0, pecaCarregada.getForca());
//                                    percorrerListaPecasArmadilhas(pecasNaArmadilhaJogador2);
                                        liberarPecas(pecasNaArmadilhaJogador2, pecaCarregada);

                                        peca.setEstaViva(false);

                                        // LOGO ABAIXO VAI O STRATEGY
                                        boolean verificarArmadilhaStrategy = (boolean) jogador2.verificarEspacoEscolhido(new VerificaArmadilha(), tabuleiro.getTabuleiroEspaco()[k][i]);
                                        // ----- FIM DO STRATEGY -----
                                        if (!verificarArmadilhaStrategy && tabuleiro.getTabuleiroEspaco()[k][i].getNome().equalsIgnoreCase("armadilha")) {
                                            pecasNaArmadilhaJogador2.add(pecaCarregada);
                                            System.out.println("Entrou na lista");
                                        }

                                        //removendo a peça capturada
                                        //jogador1.removePeca(peca);
                                        // VISITOR
                                        Visitor calcPecas = new CalcPecas();
                                        jogador1.accept(calcPecas);
//                                        System.out.println("Jogador 1 - Peças: " + jogador1.getPecas().size());
//                                        System.out.println("JOGADOR 1 - NUMERO DE PEÇAS: " + calcPecas.getNumeroPecas());
                                        if (calcPecas.getNumeroPecas() == 0) {
                                            try {
                                                tabuleiro.getEstado().proxEstado();
                                                observadores.get(0).alterarStatus("Jogo Finalizado! Você ganhou!");
                                            } catch (Exception ex) {
//                                                    Logger.getLogger(ControlePrincipal.class.getName()).log(Level.SEVERE, null, ex);
                                            }
                                            alertaJogoFinalizado("Jogo Finalizado! Você ganhou!");
                                        }
                                        return;
                                    }
                                }
                                pecaCarregada = null;
                            }
                            return;
                        }
                    }
                }
            } else {
                if (pecaCarregada != null) {
                    if (this.rodada == 0) {
                        PermissaoMovimentacao permissao = retornaPermissao(pecaCarregada);
                        if (permissao.permitir(pecaCarregada, k, i)) {

                            boolean verificarArmadilhaStrategy = (boolean) jogador1.verificarEspacoEscolhido(new VerificaArmadilha(), tabuleiro.getTabuleiroEspaco()[k][i]);

                            boolean verificarTocaStrategy = (boolean) jogador1.verificarEspacoEscolhido(new VerificaToca(), tabuleiro.getTabuleiroEspaco()[k][i]);

                            if (verificarArmadilhaStrategy && tabuleiro.getTabuleiroEspaco()[k][i].getNome().equalsIgnoreCase("armadilha")) {
//                                pecaCarregada.setPodeMovimentar(false);
                                pecasNaArmadilhaJogador1.add(pecaCarregada);
                                ordemMovimentar(pecaCarregada, k, i, 1, 0);
                            } else {
                                if (tabuleiro.getTabuleiroEspaco()[k][i].getNome().equalsIgnoreCase("toca") && verificarTocaStrategy) {
                                    try {
                                        tabuleiro.getEstado().proxEstado();
                                        observadores.get(0).alterarStatus("Jogo Finalizado! Você ganhou!");
                                    } catch (Exception ex) {
//                                                    Logger.getLogger(ControlePrincipal.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                    alertaJogoFinalizado("Jogo Finalizado! Você ganhou!");
                                    pecaCarregada = null;
                                    return;
                                } else {
                                    //Se toca for do jogador, retorna 'false'
                                    if (!(tabuleiro.getTabuleiroEspaco()[k][i].getNome().equalsIgnoreCase("toca") && !verificarTocaStrategy)) {
                                        ordemMovimentar(pecaCarregada, k, i, 1, pecaCarregada.getForca());
//                                        percorrerListaPecasArmadilhas(pecasNaArmadilhaJogador1);
                                        liberarPecas(pecasNaArmadilhaJogador1, pecaCarregada);
                                        pecaCarregada = null;
                                        return;
                                    }
                                }
                            }
                        }
                    } else {
                        PermissaoMovimentacao permissao = retornaPermissao(pecaCarregada);
                        if (permissao.permitir(pecaCarregada, k, i)) {

                            boolean verificarArmadilhaStrategy = (boolean) jogador2.verificarEspacoEscolhido(new VerificaArmadilha(), tabuleiro.getTabuleiroEspaco()[k][i]);

                            boolean verificarTocaStrategy = (boolean) jogador2.verificarEspacoEscolhido(new VerificaToca(), tabuleiro.getTabuleiroEspaco()[k][i]);

                            if (verificarArmadilhaStrategy && tabuleiro.getTabuleiroEspaco()[k][i].getNome().equalsIgnoreCase("armadilha")) {
//                                pecaCarregada.setPodeMovimentar(false);
                                pecasNaArmadilhaJogador2.add(pecaCarregada);
                                ordemMovimentar(pecaCarregada, k, i, 0, 0);
                                pecaCarregada = null;
                                return;
                            } else {
                                if (tabuleiro.getTabuleiroEspaco()[k][i].getNome().equalsIgnoreCase("toca") && verificarTocaStrategy) {
                                    try {
                                        tabuleiro.getEstado().proxEstado();
                                        observadores.get(0).alterarStatus("Jogo Finalizado! Você ganhou!");
                                    } catch (Exception ex) {
//                                                    Logger.getLogger(ControlePrincipal.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                    alertaJogoFinalizado("Jogo Finalizado! Você ganhou!");
                                    pecaCarregada = null;
                                    return;

                                } else {
                                    if (!(tabuleiro.getTabuleiroEspaco()[k][i].getNome().equalsIgnoreCase("toca") && !verificarTocaStrategy)) {
                                        ordemMovimentar(pecaCarregada, k, i, 0, pecaCarregada.getForca());
//                                        percorrerListaPecasArmadilhas(pecasNaArmadilhaJogador2);
                                        liberarPecas(pecasNaArmadilhaJogador2, pecaCarregada);
                                        pecaCarregada = null;
                                        return;
                                    }
                                }
                            }

                        }

                    }
                }

            }

        }

    }

    private String[] retornaValorInteiro(int valor) {

        String nova[] = new String[2];
        String v = valor + "";

        if (v.length() != 2) {
            nova[0] = "0";
            nova[1] = v.substring(0, 1);
        } else {
            nova[0] = v.substring(0, 1);
            nova[1] = v.substring(1, 2);
        }

        return nova;
    }
//
//    private void percorrerListaPecasArmadilhas(List<Peca> pecas) {
//        pecas.forEach((p) -> {
//            p.setPodeMovimentar(!p.getPodeMovimentar());
//        });
//    }

    private void liberarPecas(List<Peca> pecas, Peca pecaCarregada) {

        for (Peca p : pecas) {
            if (p.equals(pecaCarregada)) {
                tabuleiro.getTabuleiroEspaco()[p.getPosX()][p.getPosY()].getPeca().setForca(recuperarForca(p));
            }
//            tabuleiro.getTabuleiroEspaco()[p.getPosX()][p.getPosY()].getPeca().setForca(recuperarForca(p));
        }

        pecas.remove(pecaCarregada);

    }

    private int recuperarForca(Peca peca) {
        switch (peca.getNome()) {
            case "Cachorro":
                return 3;
            case "Elefante":
                return 8;
            case "Gato":
                return 2;
            case "Leao":
                return 7;
            case "Leopardo":
                return 5;
            case "Lobo":
                return 4;
            case "Rato":
                return 1;
            case "Tigre":
                return 6;
        }
        return 0;
    }

//    private boolean pecaLista(List<Peca> pecas, Peca peca) {
//        for (Peca p : pecas) {
//            if (p.equals(peca)) {
//                return false;
//            }
//        }
//        return true;
//    }
    private void ordemMovimentar(Peca pecaCarregada, int k, int i, int rodada, int forca) {
        pecaCarregada.setForca(forca);
        this.rodada = rodada;
        invoker = new CommandInvoker(pecaCarregada);
        try {
            invoker.execute(1, k, i);
            atualizarViewTabuleiro(this.srcAntigo, this.pecaCarregada.getSrcImg(), k, i, xAntigo, this.yAntigo);
            tirarSelecao();

        } catch (Exception ex) {
            Logger.getLogger(ControlePrincipal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void atualizarViewTabuleiro(String srcAntigo, String srcNovo, int xNovo, int yNovo, int xAntigo, int yAntigo) {
        observadores.forEach((obs) -> {
            obs.mudarImagem(srcAntigo, srcNovo, xNovo, yNovo, xAntigo, yAntigo);
        });
    }

    private void tirarSelecao() {
        observadores.forEach((obs) -> {
            obs.tirarSelecao();
        });
    }

    private PermissaoMovimentacao retornaPermissao(Peca peca) {
        PermissaoMovimentacao permPadrao = new PermissaoMovimentacaoPadrao();
        if (peca.getNome().equalsIgnoreCase("rato")) {
            return new PermissaoEntraRio(permPadrao);
        } else if (peca.getNome().equalsIgnoreCase("tigre") || peca.getNome().equalsIgnoreCase("leao")) {
            return new PermissaoPulaRio(permPadrao);
        } else {
            return permPadrao;
        }
    }

    private void alertaJogoFinalizado(String alerta) {
        observadores.forEach((obs) -> {
            obs.alertaJogoFinalizado(alerta);
        });
    }

}
