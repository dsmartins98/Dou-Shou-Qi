/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.List;
import model.strategy.VerificarEspaco;
import model.visitor.Visitor;

/**
 *
 * @author douglas
 */
public class Jogador {

    
    private List<Peca> pecas = new ArrayList<>();
    private List<Espaco> armadilhas = new ArrayList<>();
    private Espaco toca;
    private VerificarEspaco verificarEspaco;
    
    //Strategy
    public boolean verificarEspacoEscolhido(VerificarEspaco verificarEspaco, Espaco espaco){
        this.verificarEspaco = verificarEspaco;
        return this.verificarEspaco.verificar(this, espaco);
    }
    
    //Visitor
    public void accept(Visitor visitor){
        for(Peca p: this.pecas){
            visitor.visit(p);           
        }
    }

    public List<Espaco> getArmadilhas() {
        return armadilhas;
    }

    public void addArmadilhas(Espaco armadilha) {
        armadilhas.add(armadilha);
    }

    public Espaco getToca() {
        return toca;
    }

    public void setToca(Espaco espaco) {
        this.toca = espaco;
    }

    public void removePeca(Peca peca) {
        pecas.remove(peca);
    }

    public List<Peca> getPecas() {
        return pecas;
    }

    public void addPeca(Peca peca) {
        try {
            pecas.add(peca);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public Peca getPeca(String nome) {
        Peca peca = null;
        for (Peca p : pecas) {
            if (p.getNome().equalsIgnoreCase(nome)) {
                peca = p;
            }
        }

        return peca;
    }

    /**
     * MÉTODO SUBSTITUÍDO PELA IMPLEMENTAÇÃO DO VISITOR.
     */
//    public boolean verificarPeca(Peca peca) {
//        for (Peca p : pecas) {
//            if (p == peca) {
//                return true;
//            }
//
//        }
//        return false;
//    }
    
    /**
     * MÉTODO SUBSTITUÍDO PELA IMPLEMENTAÇÃO DO VISITOR.
     */
//    public boolean verificarArmadilha(Jogador jogadorGhost, Jogador jogador1, int k, int i) {
//        if (jogadorGhost == jogador1) {
//            if ((k == 0 && i == 2) || (k == 0 && i == 4) || (k == 1 && i == 3)) {
//                return true;
//            }
//        } else {
//            if ((k == 7 && i == 3) || (k == 8 && i == 3) || (k == 8 && i == 4)) {
//                return true;
//            }
//        }
//        return false;
//    }

}
