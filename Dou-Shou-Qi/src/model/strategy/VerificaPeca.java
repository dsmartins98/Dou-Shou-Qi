/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.strategy;

import model.Espaco;
import model.Jogador;
import model.Peca;

/**
 *
 * @author dougl
 */
public class VerificaPeca implements VerificarEspaco {

    @Override
    public boolean verificar(Jogador jogador, Espaco espaco) {
        try {
            for (Peca p : jogador.getPecas()) {
                if (p == espaco.getPeca()) {
                    return true;
                }
            }
        } catch (NullPointerException e) {
            return false;
        }
        return false;
    }

}
