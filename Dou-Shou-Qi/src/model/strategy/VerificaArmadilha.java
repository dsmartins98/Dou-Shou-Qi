/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.strategy;

import model.Espaco;
import model.Jogador;

/**
 *
 * @author dougl
 */
public class VerificaArmadilha implements VerificarEspaco {

    @Override
    public boolean verificar(Jogador jogador, Espaco espaco) {

        for (Espaco armadilhaJogador : jogador.getArmadilhas()) {
            if (armadilhaJogador.getX() == espaco.getX() && armadilhaJogador.getY() == espaco.getY()) {
                return false;
            }
        }
        return true;
    }

}
