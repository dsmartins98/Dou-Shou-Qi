/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.decorator;

import model.Peca;
import model.Tabuleiro;

/**
 *
 * @author dougl
 */
public class PermissaoPulaRio extends PermissaoMovimentacaoDecorator {

    public PermissaoPulaRio(PermissaoMovimentacao permissao) {
        super(permissao);
    }

    @Override
    public boolean permitir(Peca peca, int x, int y) {

        boolean verif1 = super.permitir(peca, x, y);
        
        try {

            //verifica se esta pulando em direções positivas
            if (((x == peca.getPosX() + 4) && (y == peca.getPosY())) || (((x == peca.getPosX()) && (y == peca.getPosY() + 3)))) {

                //verifica se pulou no valor x positivamente
                if (((x == peca.getPosX() + 4) && (y == peca.getPosY()))) {

                    //verifica se o espaço depois da peça é um lago
                    if (Tabuleiro.getInstance().getTabuleiroEspaco()[peca.getPosX() + 1][peca.getPosY()].getNome().equalsIgnoreCase("lago")) {

                        //verifica se os proximos espaços estão vazios (sem peças)
                        if (((Tabuleiro.getInstance().getTabuleiroEspaco()[peca.getPosX() + 1][peca.getPosY()].getPeca() == null)
                                && (Tabuleiro.getInstance().getTabuleiroEspaco()[peca.getPosX() + 2][peca.getPosY()].getPeca() == null)
                                && (Tabuleiro.getInstance().getTabuleiroEspaco()[peca.getPosX() + 3][peca.getPosY()].getPeca() == null))) {
                            return true;
                        }

                    }

                } else if (((x == peca.getPosX()) && (y == peca.getPosY() + 3))) {

                    if ((Tabuleiro.getInstance().getTabuleiroEspaco()[peca.getPosX()][peca.getPosY() + 1].getNome().equalsIgnoreCase("lago"))) {

                        if (((Tabuleiro.getInstance().getTabuleiroEspaco()[peca.getPosX()][peca.getPosY() + 1].getPeca() == null)
                                && (Tabuleiro.getInstance().getTabuleiroEspaco()[peca.getPosX()][peca.getPosY() + 2].getPeca() == null))) {
                            return true;
                        }

                    }

                }

            } else {

                //verifica se pulou no valor x positivamente
                if (((x == peca.getPosX() - 4) && (y == peca.getPosY()))) {

                    //verifica se o espaço depois da peça é um lago
                    if (Tabuleiro.getInstance().getTabuleiroEspaco()[peca.getPosX() - 1][peca.getPosY()].getNome().equalsIgnoreCase("lago")) {

                        //verifica se os proximos espaços estão vazios (sem peças)
                        if (((Tabuleiro.getInstance().getTabuleiroEspaco()[peca.getPosX() - 1][peca.getPosY()].getPeca() == null)
                                && (Tabuleiro.getInstance().getTabuleiroEspaco()[peca.getPosX() - 2][peca.getPosY()].getPeca() == null)
                                && (Tabuleiro.getInstance().getTabuleiroEspaco()[peca.getPosX() - 3][peca.getPosY()].getPeca() == null))) {
                            return true;
                        }

                    }

                } else if (((x == peca.getPosX()) && (y == peca.getPosY() - 3))) {

                    if ((Tabuleiro.getInstance().getTabuleiroEspaco()[peca.getPosX()][peca.getPosY() - 1].getNome().equalsIgnoreCase("lago"))) {

                        if (((Tabuleiro.getInstance().getTabuleiroEspaco()[peca.getPosX()][peca.getPosY() - 1].getPeca() == null)
                                && (Tabuleiro.getInstance().getTabuleiroEspaco()[peca.getPosX()][peca.getPosY() - 2].getPeca() == null))) {
                            return true;
                        }

                    }

                }

            }

            return verif1;
        } catch (IndexOutOfBoundsException e) {
            return false;
        }

    }

}
