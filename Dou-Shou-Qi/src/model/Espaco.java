/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author douglas
 */
public abstract class Espaco {

    private String nome;
    private Peca peca;
    private String srcImgEspaco;
    private int x;
    private int y;

    public Espaco(String nome, int x, int y, String src, Peca peca) {
        this.nome = nome;
        this.peca = peca;
        this.srcImgEspaco = src;
        this.x = x;
        this.y = y;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Peca getPeca() {
        return peca;
    }

    public void setPeca(Peca peca) {
        this.peca = peca;
    }

    public String getSrcImg() {
        return srcImgEspaco;
    }

    public void setSrcImg(String srcImg) {
        this.srcImgEspaco = srcImg;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public String toString() {
        return "Espaco{" + srcImgEspaco + '}';
    }

}
