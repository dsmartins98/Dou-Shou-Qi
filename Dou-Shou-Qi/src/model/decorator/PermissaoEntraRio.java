/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.decorator;

import model.Peca;

/**
 *
 * @author dougl
 */
public class PermissaoEntraRio extends PermissaoMovimentacaoDecorator {

    public PermissaoEntraRio(PermissaoMovimentacao permissao) {
        super(permissao);
    }

    @Override
    public boolean permitir(Peca peca, int x, int y) {

       return (((((x == peca.getPosX() + 1) && (y == peca.getPosY())) || ((x == peca.getPosX()) && (y == peca.getPosY() + 1)))
                || (((x == peca.getPosX() - 1) && (y == peca.getPosY())) || ((x == peca.getPosX()) && (y == peca.getPosY() - 1))))
                
                && (((!peca.getNome().equalsIgnoreCase("Rato")) 
                && (!peca.tabuleiro.getTabuleiroEspaco()[x][y].getNome().equalsIgnoreCase("lago")))
                || (peca.getNome().equalsIgnoreCase("Rato"))) 
                
                && (!peca.tabuleiro.getTabuleiroEspaco()[x][y].getNome().equalsIgnoreCase("toca")));
        
    }

}
