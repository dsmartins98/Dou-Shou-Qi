/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.state;

import model.Jogador;
import model.Peca;
import model.Tabuleiro;

/**
 *
 * @author Douglas
 */
public class TabuleiroAtivado extends TabuleiroEstado{
    
    public TabuleiroAtivado(Tabuleiro tabulerio) {
        super(tabulerio);
    }

    @Override
    public void proxEstado() throws Exception {
        tabuleiro.liberarPecas(false);
        tabuleiro.setEstado(new TabuleiroDesativado(tabuleiro));
    }

    @Override
    public void liberarPecas(boolean liberar) throws Exception {
        tabuleiro.setHabilitado(liberar);
//        for(Peca j: jogador.getPecas()){
//            j.setPodeMovimentar(!j.getPodeMovimentar());
//        }
    }
    
}
