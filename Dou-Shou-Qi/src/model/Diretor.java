/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author douglas
 */
public class Diretor {

    private BuilderJogador builderJogador;

    public Diretor(BuilderJogador builderJogador) {
        this.builderJogador = builderJogador;
    }

    public void construir(Tabuleiro tabuleiro) {

        builderJogador.constroiElefante(tabuleiro);
        builderJogador.constroiCachorro(tabuleiro);
        builderJogador.constroiGato(tabuleiro);
        builderJogador.constroiLeao(tabuleiro);
        builderJogador.constroiLeopardo(tabuleiro);
        builderJogador.constroiLobo(tabuleiro);
        builderJogador.constroiRato(tabuleiro);
        builderJogador.constroiTigre(tabuleiro);
        builderJogador.construirArmadilhas(tabuleiro);
        builderJogador.construirToca(tabuleiro);
    }

}
