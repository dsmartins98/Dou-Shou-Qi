/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author 10354209965
 */
public class CommandInvoker {

    private Map<Integer, Command> comandos = new HashMap<>();
    private Peca peca;

    public CommandInvoker(Peca peca) {
        this.peca = peca;
        comandos.put(1, new Movimentar(peca));
        
    }

    public void addCommand(int key, Command comm) {
        comandos.put(key, comm);
    }

    public void execute(int op, int x, int y) throws Exception {

        Command comm = comandos.get(op);
        comm.execute(x, y);

    }

}
