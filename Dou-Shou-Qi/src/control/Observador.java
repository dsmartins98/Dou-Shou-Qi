/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

/**
 *
 * @author douglas
 */
public interface Observador {

    public void mudarImagem(String srcAntigo, String srcNovo, int xNovo, int yNovo, int xAntigo, int yAntigo);
    
    public void tirarSelecao();
    
    public void alertaJogoFinalizado(String alerta);
    
    public void alterarStatus(String status);
    
    public void desabilitarBotaoStart();
    
}
