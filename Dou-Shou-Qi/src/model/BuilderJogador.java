/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author douglas
 */
public abstract class BuilderJogador {

    protected Jogador jogador;
    protected FabricaDePeca fabricaDePeca;

    public BuilderJogador(FabricaDePeca fabrica) {
        this.jogador = new Jogador();
        this.fabricaDePeca = fabrica;
    }

    public Jogador getJogador() {
        return jogador;
    }

    //Tabuleiro é passado como parâmetro para deixar a movimentação
    //das peças com elas próprias
    public abstract void constroiRato(Tabuleiro tabuleiro);

    public abstract void constroiCachorro(Tabuleiro tabuleiro);

    public abstract void constroiGato(Tabuleiro tabuleiro);

    public abstract void constroiLeopardo(Tabuleiro tabuleiro);

    public abstract void constroiLobo(Tabuleiro tabuleiro);

    public abstract void constroiLeao(Tabuleiro tabuleiro);

    public abstract void constroiTigre(Tabuleiro tabuleiro);

    public abstract void constroiElefante(Tabuleiro tabuleiro);
    
    public abstract void construirArmadilhas(Tabuleiro tabuleiro);
    
    public abstract void construirToca(Tabuleiro tabuleiro);

}
