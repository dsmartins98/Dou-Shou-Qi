/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import model.state.TabuleiroEstado;
import model.state.TabuleiroDesativado;

/**
 *
 * @author douglas
 */
public class Tabuleiro {

    private final int LARGURA = 9;
    private final int ALTURA = 7;
    private final Espaco tabuleiroMatriz[][] = new Espaco[LARGURA][ALTURA];
    private boolean habilitado = false;
    private TabuleiroEstado estado;

    //singleton
    private static Tabuleiro instance;

    private Tabuleiro() {

    }

    public synchronized static Tabuleiro getInstance() {

        if (instance == null) {
            instance = new Tabuleiro();
        }

        return instance;

    }

    public int getLargura() {
        return LARGURA;
    }

    public int getAltura() {
        return ALTURA;
    }

    public Espaco[][] getTabuleiroEspaco() {
        return tabuleiroMatriz;
    }

    public TabuleiroEstado getEstado() {
        return estado;
    }

    public void setEstado(TabuleiroEstado estado) {
        this.estado = estado;
    }

    public void start() {
        this.estado = new TabuleiroDesativado(this);
    }

    public void liberarPecas(boolean liberar) throws Exception {
        this.estado.liberarPecas(liberar);
    }

    public boolean isHabilitado() {
        return habilitado;
    }

    public void setHabilitado(boolean habilitado) {
        this.habilitado = habilitado;
    }

}
