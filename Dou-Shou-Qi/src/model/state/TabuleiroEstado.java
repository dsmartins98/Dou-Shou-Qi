/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.state;

import model.Jogador;
import model.Tabuleiro;

/**
 *
 * @author Douglas
 */
public abstract class TabuleiroEstado {

    protected Tabuleiro tabuleiro;

    public TabuleiroEstado(Tabuleiro tabuleiro) {
        this.tabuleiro = tabuleiro;
    }
    
    public abstract void liberarPecas(boolean liberar) throws Exception;
    
    public abstract void proxEstado() throws Exception;

}
