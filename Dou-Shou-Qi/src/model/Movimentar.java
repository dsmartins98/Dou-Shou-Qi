/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author dougl
 */
public class Movimentar implements Command {

    private Peca peca;

    public Movimentar(Peca peca) {
        this.peca = peca;
    }

    @Override
    public void execute(int valorX, int valorY) {
        peca.movimentar(valorX, valorY);
    }

}
