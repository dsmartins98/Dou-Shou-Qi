/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author douglas
 */
public class ConcretJogador2 extends BuilderJogador{
    
     public ConcretJogador2(FabricaDePeca fabrica){
        super(fabrica);
    }
    
    //Adicionando as peças do jogador2 no jogador2
    @Override
    public void constroiElefante(Tabuleiro tabuleiro) {
        jogador.addPeca(fabricaDePeca.criarElefante("Elefante", 8, "/view/imagens/elefante2.png", tabuleiro, 6, 6, jogador));
    }

    @Override
    public void constroiTigre(Tabuleiro tabuleiro) {
        jogador.addPeca(fabricaDePeca.criarTigre("Tigre", 6, "/view/imagens/tigre2.png", tabuleiro, 8, 6, jogador));
    }

    @Override
    public void constroiLeao(Tabuleiro tabuleiro) {
        jogador.addPeca(fabricaDePeca.criarLeao("Leao", 7, "/view/imagens/leao2.png", tabuleiro, 8, 0, jogador));
    }

    @Override
    public void constroiLobo(Tabuleiro tabuleiro) {
        jogador.addPeca(fabricaDePeca.criarLobo("Lobo", 4, "/view/imagens/lobo2.png", tabuleiro, 6, 4, jogador));
    }

    @Override
    public void constroiLeopardo(Tabuleiro tabuleiro) {
        jogador.addPeca(fabricaDePeca.criarLeopardo("Leopardo", 5, "/view/imagens/leopardo2.png", tabuleiro, 6 , 2, jogador));
    }

    @Override
    public void constroiGato(Tabuleiro tabuleiro) {
        jogador.addPeca(fabricaDePeca.criarGato("Gato", 2, "/view/imagens/gato2.png", tabuleiro, 7, 5, jogador));
    }

    @Override
    public void constroiCachorro(Tabuleiro tabuleiro) {
        jogador.addPeca(fabricaDePeca.criarCachorro("Cachorro", 3, "/view/imagens/cao2.png", tabuleiro, 7, 1, jogador));
    }

    @Override
    public void constroiRato(Tabuleiro tabuleiro) {
        jogador.addPeca(fabricaDePeca.criarRato("Rato", 1, "/view/imagens/rato2.png", tabuleiro, 6, 0, jogador));
    }
    
    @Override
    public void construirArmadilhas(Tabuleiro tabuleiro) {
        jogador.addArmadilhas(tabuleiro.getTabuleiroEspaco()[7][3] = new Armadilha("armadilha",7, 3, "/view/imagens/armadilha.PNG", null));
        jogador.addArmadilhas(tabuleiro.getTabuleiroEspaco()[8][2] = new Armadilha("armadilha",8, 2, "/view/imagens/armadilha.PNG", null));
        jogador.addArmadilhas(tabuleiro.getTabuleiroEspaco()[8][4] = new Armadilha("armadilha",8, 4, "/view/imagens/armadilha.PNG", null));
    }

    @Override
    public void construirToca(Tabuleiro tabuleiro) {
        jogador.setToca(tabuleiro.getTabuleiroEspaco()[8][3] = new Toca("toca",8, 3, "/view/imagens/toca.png", null));
    }
}
