/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author douglas
 */
public class FabricaConcretaDePeca extends FabricaDePeca {

    @Override
    public Rato criarRato(String nome, int forca, String srcImg, Tabuleiro tabuleiro, int posX, int posY, Jogador jogador) {
        return new Rato(nome, forca, srcImg, tabuleiro, posX, posY, jogador);
    }

    @Override
    public Gato criarGato(String nome, int forca, String srcImg, Tabuleiro tabuleiro, int posX, int posY, Jogador jogador) {
        return new Gato(nome, forca, srcImg, tabuleiro, posX, posY, jogador);
    }

    @Override
    public Cachorro criarCachorro(String nome, int forca, String srcImg, Tabuleiro tabuleiro, int posX, int posY, Jogador jogador) {
        return new Cachorro(nome, forca, srcImg, tabuleiro, posX, posY, jogador);
    }

    @Override
    public Lobo criarLobo(String nome, int forca, String srcImg, Tabuleiro tabuleiro, int posX, int posY, Jogador jogador) {
        return new Lobo(nome, forca, srcImg, tabuleiro, posX, posY, jogador);
    }

    @Override
    public Leopardo criarLeopardo(String nome, int forca, String srcImg, Tabuleiro tabuleiro, int posX, int posY, Jogador jogador) {
        return new Leopardo(nome, forca, srcImg, tabuleiro, posX, posY, jogador);
    }

    @Override
    public Tigre criarTigre(String nome, int forca, String srcImg, Tabuleiro tabuleiro, int posX, int posY, Jogador jogador) {
        return new Tigre(nome, forca, srcImg, tabuleiro, posX, posY, jogador);
    }

    @Override
    public Leao criarLeao(String nome, int forca, String srcImg, Tabuleiro tabuleiro, int posX, int posY, Jogador jogador) {
        return new Leao(nome, forca, srcImg, tabuleiro, posX, posY, jogador);
    }

    @Override
    public Elefante criarElefante(String nome, int forca, String srcImg, Tabuleiro tabuleiro, int posX, int posY, Jogador jogador) {
        return new Elefante(nome, forca, srcImg, tabuleiro, posX, posY, jogador);
    }

}
